/*
  https://gitlab.com/Pelado-Mat/notanks-telemetry/
  
  RC Voltage and Temp Sensor with PWM Fan control

  Not for commercial use
  BOM:
    - Arduino Pro-Mini 5V
    - 2x 100K NTC Thermistors
    - 2x 100K Resistors
    - 3x 680K Resistor
    - 1x 300K Resistor
    - 1x 51K Resistor
    - Jumper R8 D16 Reciever
    - 8025 12v CPU Fan - AVC DS08025T12U

  Sources:
    https://fdossena.com/?p=ArduinoFanControl/i.md
    https://www.rcgroups.com/forums/showthread.php?2245978-FrSky-S-Port-telemetry-library-easy-to-use-and-configurable

*/

#include "FrSkySportSensor.h"
#include "FrSkySportSensorEsc.h"
#include "FrSkySportSensorFlvss.h"
#include "FrSkySportSensorRpm.h"
#include "FrSkySportSingleWireSerial.h"
#include "FrSkySportTelemetry.h"
#include "math.h"
#if !defined(TEENSY_HW)
#include "SoftwareSerial.h"
#endif

//#define DEBUG

#define MAX_CELLS 3
#define MAX_THERM 2

// Sensors
FrSkySportSensorFlvss flvss1; // Create FLVSS sensor with default ID
FrSkySportSensorRpm rpm; // Create RPM sensor with default ID
FrSkySportTelemetry telemetry;

#define VENT_RPM_PIN 2
#define VENT_PWM_PIN 3
#define DEBOUNCE                                                               \
  0 // 0 is fine for most fans, crappy fans may require 10 or 20 to filter out
    // noise
#define FANSTUCK_THRESHOLD                                                     \
  500 // if no interrupts were received for 500ms, consider the fan as stuck and
      // report 0 RPM

const int cell_pin[MAX_CELLS] = {A2, A1, A0};

// (R2+R1)/R2
// 1S - R1: - R2: 51k
// 2S - R1: 680K - R2: 680K
// 3S - R1: 680K - R2: 300K

const float cell_fact[MAX_CELLS] = {1.0000, 2.0000, 3.26666667};

const int therm_pin[MAX_THERM] = {A4, A5};

const float therm_k[MAX_THERM] = {10000.0000, 10000.0000};

float CELL_V[MAX_CELLS];

float TEMP[MAX_THERM];

float readVolt = 0;
float cellVolt = 0;

float VCC_VOLT = 5.0;
unsigned long myMillis = 0;
unsigned long vent_rpm = 0;

void setupTimerPin3() {
  // Set PWM frequency to about 25khz on pin 3 (timer 2 mode 5, prescale 8,
  // count to 79)
  TIMSK2 = 0;
  TIFR2 = 0;
  TCCR2A = (1 << COM2B1) | (1 << WGM21) | (1 << WGM20);
  TCCR2B = (1 << WGM22) | (1 << CS21);
  OCR2A = 79;
  OCR2B = 0;
}

// equivalent of analogWrite on pin 3
void setPWMPin3(float f) {
  f = f < 0 ? 0 : f > 1 ? 1 : f;
  OCR2B = (uint8_t)(79 * f);
}

// Interrupt handler. Stores the timestamps of the last 2 interrupts and handles
// debouncing
unsigned long volatile ts1 = 0, ts2 = 0;
void tachISR() {
  unsigned long m = millis();
  if ((m - ts2) > DEBOUNCE) {
    ts1 = ts2;
    ts2 = m;
  }
}
// Calculates the RPM based on the timestamps of the last 2 interrupts. Can be
// called at any time.
unsigned long calcRPM() {
  if (millis() - ts2 < FANSTUCK_THRESHOLD && ts2 != 0) {
    return (60000 / (ts2 - ts1)) / 2;
  } else
    return 0;
}

long readVcc() {
// Read 1.1V reference against AVcc
// set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) ||              \
    defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined(__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) ||                \
    defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined(__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) ||                \
    defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  delay(2);            // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA, ADSC))
    ; // measuring

  uint8_t low = ADCL;  // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high << 8) | low;

  result =
      1116386.7327 / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000

  return result; // Vcc in millivolts
}

float readTemp(int PIN, float R1) {
  const float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
  float logR2, T;
  logR2 = log(R1 * (1023.0 / (float)analogRead(PIN) - 1.0));
  T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2)) - 273.15;
  return T;
}

void setup() {
  pinMode(VENT_PWM_PIN, OUTPUT);
  setupTimerPin3();
  setPWMPin3(0);
  pinMode(VENT_RPM_PIN,
          INPUT_PULLUP); // set the sense pin as input with pullup resistor
  attachInterrupt(digitalPinToInterrupt(VENT_RPM_PIN), tachISR,
                  FALLING); // set tachISR to be triggered when the signal on
                            // the sense pin goes low
#ifdef DEBUG
  // open the serial port at 9600 bps:
  Serial.begin(9600);
#endif

  // Configure the telemetry serial port and sensors (remember to use & to
  // specify a pointer to sensor)
#if defined(TEENSY_HW)
  telemetry.begin(FrSkySportSingleWireSerial::SERIAL_3, &flvss1, &rpm);
#else
  telemetry.begin(FrSkySportSingleWireSerial::SOFT_SERIAL_PIN_6, &flvss1, &rpm);
#endif
}

void loop() {
  int max_temp = 0;
  float vent_speed = 0;
  // Read sensors every .5s
  // Read Cell Voltage
  // Set FAN Speed
  if ((millis() - myMillis) > 500) {
    // Calibrate VCC Voltage
    myMillis = millis();
    VCC_VOLT = (float)readVcc() / 1000.0;
#ifdef DEBUG
    Serial.print("#### VCC Voltage: ");
    Serial.print(VCC_VOLT);
    Serial.println(" V");
#endif
    // Check temp
    for (int i = 0; i < MAX_THERM; i++) {
      TEMP[i] = readTemp(therm_pin[i], therm_k[i]);
      if (i == 0) {
        max_temp = TEMP[i];
      } else {
        max_temp = max(TEMP[i], max_temp);
      }
#ifdef DEBUG
    Serial.print("Therm ");
    Serial.print(i + 1);
    Serial.print(" :");
    Serial.print(TEMP[i]);
    Serial.println("C");
#endif
    };
    // Set Fan Speed
    if (max_temp > 90) {
      vent_speed = 1;
    } else if (max_temp > 70) {
      vent_speed = 0.5;
    } else if (max_temp > 50) {
      vent_speed = 0.2;
    } else {
      vent_speed = 0;
    };
    setPWMPin3(vent_speed);
#ifdef DEBUG
    Serial.print("MAX Temp ");
    Serial.print(" :");
    Serial.print(max_temp);
    Serial.println("C");
    Serial.print("Set PWM ");
    Serial.print(" :");
    Serial.println(vent_speed);
#endif
    // Read Vent RPM
    vent_rpm = calcRPM();

#ifdef DEBUG
    Serial.print("Vent RPM ");
    Serial.print(" :");
    Serial.println(vent_rpm);
#endif

    // CELL VOLTAGE
    for (int i = 0; i < MAX_CELLS; i++) {
      readVolt = (float)analogRead(cell_pin[i]) * VCC_VOLT / 1023;
      cellVolt = readVolt * cell_fact[i];
      CELL_V[i] = cellVolt;
      for (int j = i; j > 0; j--) {
        CELL_V[i] -= CELL_V[j - 1];
      };
#ifdef DEBUG
      Serial.print("Cell Voltage ");
      Serial.print(i + 1);
      Serial.print(" :");
      Serial.print(readVolt);
      Serial.print("V - ");
      Serial.print(cellVolt);
      Serial.print("V - ");
      Serial.print(CELL_V[i]);
      Serial.println("V");
#endif
    };
  };

  // Set LiPo voltage sensor (FLVSS) data (we use two sensors to simulate 8S
  // battery (set Voltage source to Cells in menu to use this data for battery
  // voltage)
  flvss1.setData(CELL_V[0], CELL_V[1],
                 CELL_V[2]); // Cell voltages in volts (cells 1-6)

  // Set RPM/temperature sensor data
  // (set number of blades to 2 in telemetry menu to get correct rpm value)
  rpm.setData(vent_rpm, // Rotations per minute
              TEMP[0],  // Temperature #1 in degrees Celsuis (can be negative,
                        // will be rounded)
              TEMP[1]); // Temperature #2 in degrees Celsuis (can be negative,
                        // will be rounded)

  // Send the telemetry data, note that the data will only be sent for sensors
  // that are being polled at given moment
  telemetry.send();
}
