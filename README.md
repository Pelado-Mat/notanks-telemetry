A simple circuit to get basic telemetry needed for a RC FPV tank
- Battery Voltage
- Motor Temperature
- PWM Fan Control

  RC Voltage and Temp Sensor with PWM Fan control
       
  Not for commercial use

  BOM:
    - Arduino Pro-Mini 5V
    - 2x 100K NTC Thermistors
    - 2x 100K Resistors   
    - 3x 680K Resistor  
    - 1x 300K Resistor
    - 1x 51K Resistor
    - Jumper R8 D16 Reciever
    - 8025 12v CPU Fan - AVC DS08025T12U
      
  Sources:
    https://fdossena.com/?p=ArduinoFanControl/i.md
    https://www.rcgroups.com/forums/showthread.php?2245978-FrSky-S-Port-telemetry-library-easy-to-use-and-configurable
